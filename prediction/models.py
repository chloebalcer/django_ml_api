from django.db import models


class YoutubeData(models.Model):
    duration = models.FloatField(null=True, blank=True,default=0.0)
    width = models.IntegerField()
    height = models.IntegerField()
    bitrate = models.IntegerField()
    framerate = models.FloatField(null=True, blank=True,default=0.0)
    i = models.IntegerField()
    p = models.IntegerField()
    b = models.IntegerField()
    frames = models.IntegerField()
    i_size = models.IntegerField()
    p_size = models.IntegerField()
    size = models.IntegerField()
    o_bitrate = models.IntegerField()
    o_framerate = models.FloatField(null=True, blank=True,default=0.0)
    o_width = models.IntegerField()
    o_height= models.IntegerField()
    umem = models.IntegerField()
    codec_flv = models.IntegerField()
    codec_h264 = models.IntegerField()
    codec_mpeg4 = models.IntegerField()
    codec_vp8 = models.IntegerField()
    o_codec_flv = models.IntegerField()
    o_codec_h264 = models.IntegerField()
    o_codec_mpeg4 = models.IntegerField()
    o_codec_vp8 = models.IntegerField()
    utime = models.FloatField(null=True, blank=True,default=0.0)


