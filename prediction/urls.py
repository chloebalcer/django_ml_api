from django.urls import path
from prediction import views

urlpatterns = [
    path('videos/', views.YoutubeDataList.as_view()),
    path('videos/<int:pk>', views.youtubeData_detail),
    path('predict/', views.PredictionView.as_view())
]
