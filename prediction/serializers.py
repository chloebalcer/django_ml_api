from rest_framework import serializers
from .models import YoutubeData

class YoutubeDataSerializer(serializers.Serializer):
    duration = serializers.FloatField()
    width = serializers.IntegerField()
    height = serializers.IntegerField()
    bitrate = serializers.IntegerField()
    framerate = serializers.FloatField()
    i = serializers.IntegerField()
    p = serializers.IntegerField()
    b = serializers.IntegerField()
    frames = serializers.IntegerField()
    i_size = serializers.IntegerField()
    p_size = serializers.IntegerField()
    size = serializers.IntegerField()
    o_bitrate = serializers.IntegerField()
    o_framerate = serializers.FloatField()
    o_width = serializers.IntegerField()
    o_height = serializers.IntegerField()
    umem = serializers.IntegerField()
    codec_flv = serializers.IntegerField()
    codec_h264 = serializers.IntegerField()
    codec_mpeg4 = serializers.IntegerField()
    codec_vp8 = serializers.IntegerField()
    o_codec_flv = serializers.IntegerField()
    o_codec_h264 = serializers.IntegerField()
    o_codec_mpeg4 = serializers.IntegerField()
    o_codec_vp8 = serializers.IntegerField()
    utime = serializers.FloatField(required=False)

    def create(self, validated_data):
        """
        Create and return a new youtube video analysis instance, give the validated data
        """
        return YoutubeData.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """ Update ans return an existing 'YoutubeData' instance, given the validated data"""
        instance.duration = validated_data.get('duration', instance.duration)
        instance.width = validated_data.get('width', instance.width)
        instance.height = validated_data.get('height', instance.height)
        instance.bitrate = validated_data.get('bitrate', instance.bitrate)
        instance.framerate = validated_data.get('framerate', instance.framerate)
        instance.i = validated_data.get('i', instance.i)
        instance.p = validated_data.get('p', instance.p)
        instance.b = validated_data.get('b', instance.b)
        instance.frames = validated_data.get('frames', instance.frames)
        instance.i_size = validated_data.get('i_size', instance.i_size)
        instance.p_size = validated_data.get('p_size', instance.p_size)
        instance.size = validated_data.get('size', instance.size)
        instance.o_bitrate = validated_data.get('o_bitrate', instance.o_bitrate)
        instance.o_framerate = validated_data.get('o_framerate', instance.o_framerate)
        instance.o_width = validated_data.get('o_width', instance.o_width)
        instance.o_height = validated_data.get('o_height', instance.o_height)
        instance.umem = validated_data.get('umem', instance.umem)
        instance.codec_flv = validated_data.get('codec_flv', instance.codec_flv)
        instance.codec_h264 = validated_data.get('codec_h264', instance.codec_h264)
        instance.codec_mpeg4 = validated_data.get('codec_mpeg4', instance.codec_mpeg4)
        instance.codec_vp8 = validated_data.get('codec_vp8', instance.codec_vp8)
        instance.o_codec_flv = validated_data.get('o_codec_flv', instance.o_codec_flv)
        instance.o_codec_h264 = validated_data.get('o_codec_h264', instance.o_codec_h264)
        instance.o_codec_mpeg4 = validated_data.get('o_codec_mpeg4', instance.o_codec_mpeg4)
        instance.o_codec_vp8 = validated_data.get('o_codec_vp8', instance.o_codec_vp8)

        instance.save()
        return instance
