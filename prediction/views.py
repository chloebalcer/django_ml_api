from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from prediction.models import YoutubeData
from prediction.serializers import YoutubeDataSerializer
from rest_framework.views import APIView
import os


class YoutubeDataList(APIView):
    def get(self, request):
        video = YoutubeData.objects.all()
        serializer = YoutubeDataSerializer(video, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = YoutubeDataSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)


@csrf_exempt
def youtubeData_detail(request, pk):
    try:
        video = YoutubeData.objects.get(pk=pk)
    except YoutubeData.DoesNotExist:
        return HttpResponse(status=404)
    if request.method == 'GET':
        serializer = YoutubeDataSerializer(video)
        return JsonResponse(serializer.data)
    elif request.method == 'PUT':
        data = JSONParser.parse(request)
        serializer = YoutubeDataSerializer(video, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)
    elif request.method == 'DELETE':
        video.delete()
        return HttpResponse(status=204)


class PredictionView(APIView):
    """ Get prediction given a specific youtube video instance"""
    def post(self, request):
        data = request.data
        serializer = YoutubeDataSerializer(data=data)
        if serializer.is_valid():
            data['utime'] = predict_utime(data)
            serializer = YoutubeDataSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=201)
            return Response(serializer.errors, status=400)
        return Response(serializer.errors, status=400)


def predict_utime(unscaled_data):
    import joblib
    import numpy as np
    modulePath = os.path.dirname(__file__)  # get current directory
    path_to_model = os.path.join(modulePath, 'xgb_model_new.pkl')
    path_to_scaler = os.path.join(modulePath, 'scaler.pkl')
    input_data = np.array(list(unscaled_data.values()))
    input = input_data.reshape((1, -1))
    scalers = joblib.load(path_to_scaler)
    scaled_input = scalers.fit_transform(input)
    model = joblib.load(path_to_model)
    utime = model.predict(scaled_input)
    return utime
