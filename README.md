# Final Project - Python For Data Analysis

## Building a Machine Learning API with django
---

### Our mission

Given a [dataset](https://archive.ics.uci.edu/ml/datasets/Online+Video+Characteristics+and+Transcoding+Time+Dataset) that contains data about some characteristics of Youtube videos, we aim to find the best model that fits our data. More particularly, we want to predict the transcoding time also called **utime**. Our target value being a continuous value, we face a regression problem. Therefore, we will try to test some regression models and compare their performance ( R2, MAE, MSE, RMSE). Once we find the right model, the goal is to create a Django api. The API will allow us to send some new data and make predictions about it. 

## Assignment

* Make a powerpoint that explains the project, and the logic behind our feature engineering and data preprocessing. 
* Write a jupyter notebook with all data preprocessing steps, data exploration, data visualization and regressions models.
* Make an api with Django

## Dataset Description

Our dataset `transcoding_mesurment.tsv` contains 10 columns of fundamental
video characteristics for 1.6 million youtube videos; It contains YouTube video id,
duration, bitrate(total in Kbits), bitrate(video bitrate in Kbits),
height(in pixle), width(in pixles), framrate, estimated framerate, codec,
category, and direct video link. This dataset can be used to gain insight
in characteristics of consumer videos found on UGC(Youtube).

Attribute Information:

id = Youtube videp id
duration = duration of video
bitrate bitrate(video) = video bitrate
height = height of video in pixles
width = width of video in pixles
frame rate = actual video frame rate
frame rate(est.) = estimated video frame rate
codec = coding standard used for the video
category = YouTube video category
url = direct link to video (has expiration date)
i = number of i frames in the video
p = number of p frames in the video
b = number of b frames in the video
frames = number of frames in video
i_size = total size in byte of i videos
p_size = total size in byte of p videos
b_size = total size in byte of b videos
size = total size of video
o_codec = output codec used for transcoding
o_bitrate = output bitrate used for transcoding
o_framerate = output framerate used for transcoding
o_width = output width in pixel used for transcoding
o_height = output height used in pixel for transcoding
umem = total codec allocated memory for transcoding
utime = total transcoding time for transcoding


## Data exploration
---

As stated before, our dataset contains 20 columns with various information about Youtube videos.
The first was for me to check for missing values (NA and null  values).
Then, I made some analysis on features to know more about data correlations.

## Models
---

For this project we aimed to predict the trancoding time also called `utime`.
For the data preprocessing steps, I made sure to transpose the categorical values (o_codec and codec)
into numeric values with the function get_dummies.

Then, I trained the models with **Linear Regression**, **Random Forest** and **XGBoost** algorithms.

To compare the performance of the trained models I focused on the following metrics:
* R2 score
* MSE
* MAE
* RMSE

### Lasso Regression
--- 
I began with Lasso Regression in order to perform feature selection and distill the features that contain the most useful information while eliminatinf as much noise as possible.
After analysing the model results, I obtain poor performance. A R² score of 0.60 and a too signicant Mean Squared Error (103).
That was an indication that the data is not linearly seperable. Yet, it was interesting to compute the lasso coefficients given the above so that we can compare them with further exmploration.

`
Results:
Linear Regression Performance
MAE 6.161379790230028
MSE 103.77867652291972
RMSE 10.18718197162099
R^2 = 0.6036236319305129`


### Random Forest
---
Then, we tried another model : Random Forest and obtained the following results which are far better than the previous ones:

`
Random Forest Regression Performance
MAE 0.0009816115432194118
MSE 0.0006092903799119936
RMSE 0.02468380805127105
R^2 = 0.9999978246222534
`

### XGBoost
---
Finally, we ended by implementing the Xtreme Gradient Boosting algorithm with performed well and gave the results below:
`
XGBoost Performance:
MSE = 9.618015718117677e-11
R^2 = 0.9999999994933025
`
Eager to optimize this model, I performed hyperparameters fine tuning by feeding sklearn GridSeachCV with some hyperparameters relevant parameters ranges and obtained the following configuration as being the optimal one:

`
{'colsample_bytree': 0.7,
 'learning_rate': 0.1,
 'max_depth': 3,
 'min_child_weight': 1,
 'n_estimators': 500,
 'objective': 'reg:squarederror',
 'subsample': 0.7}`
 
By training our data with tuned parameters I obtained the following results:

`
Tuned XGBoost Performance:
MSE = 0.04984944927597854
R^2 = 0.9998220051933407
`
The performance went worse, the range must have not been adequate.
I thus decided to stick we the first found xgboost model.

## Conclusion
---
To conclude, I manage to make some prediction using the created django api 
that gave us the results that can be observed on the image below:

 ![Image](images/prediction-result.PNG)

